package hello1

import "testing"

func TestHello1(t *testing.T) {
	want := "Hello, world from Hello1 file"
	if got := Hello1(); got != want {
		t.Errorf("Hello() = %q, want %q", got, want)
	}

}
