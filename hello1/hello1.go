package hello1

import (
	"encoding/json"
	"flag"
	"fmt"

	"gitlab/hooks/util"
)

type encryptedPassword struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

var appConfig encryptedPassword

func Hello1() string {
	return "Hello, world from Hello1 file"
}

func Hello1Print() string {
	return "No tests for this"
}

func GetPassword() string {

	secret, stage := initFlag()
	fmt.Println("stage", stage)

	byteValue, err := util.ReadFile(getConfigFile())
	fmt.Printf("%T\n", byteValue)
	if err != nil {
		fmt.Sprintf("unable to read config file, %v", err)
	}

	err = json.Unmarshal(byteValue, &appConfig)

	fmt.Println(appConfig)
	if err != nil {
		fmt.Sprintf("unable to decode into struct, %v", err)
	}

	//shared.GlobalConfig =
	adminPass, err := util.DecodeAndDecrypt(appConfig.Password, *secret)
	if err != nil {
		err.Error()
	}
	password := string(adminPass)
	fmt.Println("password", password)
	return password
	//read from json file
	//get the encrypted password
	//call decrypt function
}

func initFlag() (*string, *string) {
	secret := flag.String("secret", "", "secret string")
	stage := flag.String("stage", "", "stage string")
	flag.Parse()

	return secret, stage
}

func getConfigFile() (configFile string) {
	return "asset/config/config_application.json"
}
