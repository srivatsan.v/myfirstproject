package main

import (
	"fmt"
	"gitlab/hooks/hello"
	"gitlab/hooks/hello1"
)

func main() {
	fmt.Println(hello.Hello())
	fmt.Println(hello1.Hello1())

	fmt.Println(hello1.GetPassword())
}
