package util

import (
	"crypto/aes"
	"crypto/cipher"
	"encoding/base64"
	"errors"
	"io/ioutil"
	"os"
)

// ReadFile - read file using file util and return byte array
func ReadFile(fileName string) ([]byte, error) {
	file, err := os.Open(fileName)
	if err != nil {
		return nil, err
	}

	defer file.Close()
	return ioutil.ReadAll(file)
}

func DecodeAndDecrypt(ciphertext string, salt string) ([]byte, error) {
	ciphertext = string(DecodeBase64String(ciphertext))
	bytetext := []byte(ciphertext)
	key := []byte(salt)

	c, err := aes.NewCipher(key)
	if err != nil {
		return nil, err
	}

	gcm, err := cipher.NewGCM(c)
	if err != nil {
		return nil, err
	}

	nonceSize := gcm.NonceSize()
	if len(bytetext) < nonceSize {
		return nil, errors.New("ciphertext too short")
	}

	nonce, bytetext := bytetext[:nonceSize], bytetext[nonceSize:]
	return gcm.Open(nil, nonce, bytetext, nil)
}

// Decrypt cipher text using key
func Decrypt(ciphertext string, salt string) ([]byte, error) {
	bytetext := []byte(ciphertext)
	key := []byte(salt)

	c, err := aes.NewCipher(key)
	if err != nil {
		return nil, err
	}

	gcm, err := cipher.NewGCM(c)
	if err != nil {
		return nil, err
	}

	nonceSize := gcm.NonceSize()
	if len(bytetext) < nonceSize {
		return nil, errors.New("ciphertext too short")
	}

	nonce, bytetext := bytetext[:nonceSize], bytetext[nonceSize:]
	return gcm.Open(nil, nonce, bytetext, nil)
}

//DecodeBase64String decode base64 string
func DecodeBase64String(encodedStr string) []byte {
	decStr, err := base64.StdEncoding.DecodeString(encodedStr)
	if nil != err {
		return nil
	}
	return decStr
}
